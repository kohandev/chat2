describe('showMessage', function () {
    let sandbox= null;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('should call fn createRow onetime', () => {
        const stub = sandbox.stub(window, 'getElementById');
        serverMsg = {
            userName: 'NAME',
            body: 'message',
        };

        const actual = showMessage(serverMsg);

        sandbox.assert.calledOnce(stub);
        // const expected = 'NAME: message';
        // assert.equal(actual, expected);
    });
});