var socket = new WebSocket('ws://localhost:3000');

var chatUserName = null;
var chatRoom = 1;
var message = {
    messageType: '',
    userName: '',
    userId: null,
    room: 1,
    body: '',
};
var owner = false;
var listU = false;
var edit = false;
var editMsgId = null;

//******** авторизация *******************************

document.forms.registration.onsubmit = function (){
    chatUserName = this.nameuser.value;
    hideCover();
    showUserName(chatUserName);
    registrationUser();
    return false;
};

document.forms.publish.onsubmit = function () {
    if (!edit){
        message.messageType = 'message';
        message.userName = chatUserName;
        message.room = chatRoom;
        message.body = this.text.value;
        socket.send(JSON.stringify(message));
    } else {
        sendEditMessage();
    }
    let msgInput = document.getElementById('message-input');
    msgInput.value = '';
    return false;
};

socket.onmessage = function (event) {
    let serverMessage = JSON.parse(event.data);
    console.log(`message: ${serverMessage}`);
    switch (serverMessage.messageType) {
        case "editMsg":
            editMsgById(serverMessage);
            break;
        case "deleteMsg":
            deleteMsgById(serverMessage);
            break;
        case "allUser":
            listAllUser(serverMessage);
            break;
        case "owner":
            ownerRoom(serverMessage);
            break;
        case "listRoom":
            refreshRoomList(serverMessage);
            break;
        case "already":
            alreadyRegistration();
            break;
        case "message":
            showMessage(serverMessage);
            break;
        case "list":
            refreshUserList(serverMessage);
            break;
        default:
            console.log(`нераспознано: ${serverMessage}`);
            break
    }
    return false;
};

const invToRoom = document.getElementById('list-button');
const addRoom = document.getElementById('rooms-button');
const listRooms = document.getElementById('rooms');
const listUsers = document.getElementById('list');
const chat = document.getElementById('chat');

invToRoom.addEventListener('click', inviteToRoom);
addRoom.addEventListener('click', roomAdd);
listRooms.addEventListener('click', changeRoom);
listUsers.addEventListener('click', inviteUser);
chat.addEventListener('click', editMessage);
chat.addEventListener('contextmenu', deleteMessage);
window.addEventListener("contextmenu", (event) => event.preventDefault(), false);

function editMessage(event) {
    let buttonInput = document.getElementById('message-button');
    let msgInput = document.getElementById('message-input');
    if (edit){
        msgInput.value = '';
        buttonInput.value = 'ОТПРАВИТЬ';
        edit = false

    } else {
        editMsgId = Number(event.target.id);
        let editText = event.target.textContent;
        editText = editText.slice(editText.indexOf(':') + 2);
        msgInput.value = editText;
        buttonInput.value = 'ИЗМЕНИТЬ';
        edit = true
    }
}
function sendEditMessage() {
    let msgInput = document.getElementById('message-input');
    socket.send(JSON.stringify({
        messageType: 'editMessage',
        msgId: editMsgId,
        body: msgInput.value,
    }));

    let buttonInput = document.getElementById('message-button');
    buttonInput.value = 'ОТПРАВИТЬ';
    return false;
}

function editMsgById(serverMessage) {
    let div = document.getElementById(serverMessage.msgId);
    div.textContent = `${serverMessage.name}: ${serverMessage.body}`;
}

function deleteMessage(event) {
    socket.send(JSON.stringify({
        messageType: 'deleteMessage',
        msgId: Number(event.target.id),
    }));
}

function deleteMsgById(serverMessage) {
    let div = document.getElementById(serverMessage.msgId);
    div.remove();
}

function inviteToRoom() { //приглашение в ROOM
    let buttonOwner = document.getElementById('list-button');
    // let buttonExit = document.getElementById('list-exit');
    if (owner){
        if(listU){
            buttonOwner.value = 'ПРИГЛАСИТЬ';
            listU = false;
            message.messageType = 'list';
            socket.send(JSON.stringify(message));
        } else {
            buttonOwner.value = 'В ЧАТ';
            listU = true;
            message.messageType = 'allUser';
            socket.send(JSON.stringify(message));
        }
    }
}

function inviteUser(event) {
    let userId = event.target.id;
    if (event.target.id != 'list'){
        socket.send(JSON.stringify({
            messageType: 'addUserToRoom',
            room: chatRoom,
            userId: event.target.id,
        }));
    }
}

//*****************************************************
checkName();

function checkName() {
// let checkName = () => {
    if (!chatUserName){
        showCover();
    }
    return false;
}
function showCover () { // затеняем страницу
    let coverDiv = document.createElement('div');
    coverDiv.id = 'cover-div';
    document.body.style.overflow ='hidden';
    document.body.append(coverDiv);
    document.getElementById('name-container').style.display = 'block';

    if (document.getElementById('name-client')){
        document.getElementById('name-client').remove();
    }
}
function hideCover() { // снимаем затенение страницы
    // document.getElementById('cover-div').remove();
    document.getElementById('cover-div').remove();
    document.body.style.overflowY = '';
    document.getElementById('name-container').style.display = 'none';
}
function showUserName(userName) {
    let nameContainer = document.getElementById('form-registration');
    let nameElement = document.createElement('div');
    nameElement.className = 'name__client';
    nameElement.id = 'name-client';
    nameElement.appendChild(document.createTextNode(userName));
    nameContainer.prepend(nameElement);
}

//***************************************
function alreadyRegistration() {
    console.log(` Отказ, пользователь ${chatUserName} уже в чате`);
    alert(`Отказ, ${chatUserName} уже в чате`);
    chatUserName = null;
    checkName();
}
function registrationUser() { // отправляет на сервер данные юзера
    message.messageType = 'registration';
    message.userName = chatUserName;
    message.room = chatRoom;
    socket.send(JSON.stringify(message));
    // console.log(JSON.stringify(message));
}
function showMessage(serverMessage) {
    let message = `${serverMessage.userName}: ${serverMessage.body}`;
    let messageContainer = document.getElementById('chat');
    let messageElement = document.createElement('div');
    messageElement.id = serverMessage.idMsg;
    if (serverMessage.userName === chatUserName){
        messageElement.className = 'chat__message-own';
    } else {
        messageElement.className = 'chat__message';
    }
    messageElement.appendChild(document.createTextNode(message));
    messageContainer.appendChild(messageElement);

    return message;
}

//***** List Users ***************************************
function refreshUserList(serverMessage) {
    let listUsers = serverMessage.body;
    let userList = document.getElementById('list');
    while (userList.firstChild){ // очищаем список
        userList.removeChild(userList.firstChild);
    }
     for (var user in listUsers){
         // if (listUsers[user].room == chatRoom){ // проверка комнаты
             if (listUsers[user].name){
                 var userElement = document.createElement('div');
                 if (listUsers[user].rooms == chatRoom){
                     userElement.className = 'list__in-room';
                 } else {
                     userElement.className = 'list__not-room';
                 }
                 var userSpan = document.createElement('span');
                 if (listUsers[user].status == 'online'){ // проверка статуса
                     userSpan.className = "list__green";
                 } else {
                     userSpan.className = "list__red";
                 }
                 userSpan.appendChild(document.createTextNode('● '));
                 userElement.appendChild(userSpan);
                 userElement.appendChild(document.createTextNode(listUsers[user].name));
                 userList.appendChild(userElement);
             }
         // }
     }
     // if (serverMessage.owner == chatUserName){
     //     let buttonInvite = document.getElementById('list-button');
     //     buttonInvite.value = 'ДОБАВИТЬ';
     // }
     // console.log(serverMessage.body);
}
function listAllUser(serverMessage) {
    let listUsers = serverMessage.body;
    let userList = document.getElementById('list');
    while (userList.firstChild){ // очищаем список
        userList.removeChild(userList.firstChild);
    }
    for (var user in listUsers) {
        if (listUsers[user].name) {
            var userElement = document.createElement('div');
            userElement.className = 'list__not-room';
            userElement.id = listUsers[user].id;
            userElement.appendChild(document.createTextNode(listUsers[user].name));
            userList.appendChild(userElement);
        }
    }
}

//***** ROOMS *****************************************************
function roomAdd() { //запос на добавление ROOM
    message.messageType = 'addRoom';
    // console.log(JSON.stringify(message));
    socket.send(JSON.stringify(message));
}
function ownerRoom(serverMessage) {
    let buttonOwner = document.getElementById('list-button');
    let buttonExit = document.getElementById('list-exit');

    if (serverMessage.body == chatUserName){
        buttonOwner.disabled = false;
        buttonExit.disabled = true;
        owner = true;
    } else {
        buttonOwner.disabled = true;
        buttonExit.disabled = false;
        owner = false;
    }
}
function refreshRoomList(serverMessage) {
    let roomContainer = document.getElementById('rooms');
    while (roomContainer.firstChild){ // очищаем список
        roomContainer.removeChild(roomContainer.firstChild);
    }
    let list = serverMessage.body;
    for (let key in list){
        // roomList(list[key].room);
        let roomElement = document.createElement('div');
        roomElement.id = list[key].room;
        if (chatRoom == list[key].room){
            roomElement.className = 'rooms__select';
        } else {
            roomElement.className = 'rooms__normal';
        }

        roomElement.appendChild(document.createTextNode(`ROOM#${list[key].room}`));
        roomContainer.appendChild(roomElement);
    }
}
function changeRoom(event) {
    if (event.target.className != 'rooms'){
        chatRoom = event.target.id;
    } else {
        return false;
    }

    message.messageType = 'room';
    message.room = chatRoom;

    socket.send(JSON.stringify(message));
    // console.log(message);
    message.messageType = 'owner';
    socket.send(JSON.stringify(message));
    let messageContainer = document.getElementById('chat');
    while (messageContainer.firstChild){
        messageContainer.removeChild(messageContainer.firstChild);
    }

    let rooms = event.target.parentElement;
    for (let div of rooms.children){
        // console.log(rooms);
        // console.log(div);
        if (div.id == event.target.id){
            div.className = "rooms__select";
        } else {
            div.className = "rooms__normal";
        }
        // console.log(div);
    }


}
