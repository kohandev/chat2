var WebSocketServer = new require('ws');

var clients = {};
var storage = []; // для каждого сообщения {id}{room}{body:{name}{msg}}
var lastRoom = 1;
var rooms = {};

function Room() {
    this.owner = null;
    this.members = [];
}
rooms[1] = new Room();

function Client(){
    this.connect = null;
    this.id = null;
    this.name = null;
    this.room = 1;
    this.status = null;
}

var webSocketServer = new WebSocketServer.Server({port:3000});

webSocketServer.on('connection', function (ws) {
    var id = Math.random();
    // console.log('Новое соединение создано ' + id);

    ws.on('message', function (message) {
        // console.log(`Получено новое сообщение ${message}`);

        let clientMsg = JSON.parse(message);
        switch (clientMsg.messageType) {
            case "editMessage":
                editMessage(clientMsg);
                break;
            case "deleteMessage":
                deleteMessage(clientMsg.msgId);
                break;
            case "addUserToRoom":
                addUserToRoom(clientMsg);
                break;
            case "list":
                sendRoomClientList(clients[id].room);
                break;
            case "allUser":
                sendAllNoRoom();
                break;
            case "owner":
                sendOwnerRoom();
                break;
            case "addRoom":
                addNewRoom();
                break;
            case "registration":
                registrationClient(clientMsg);
                if (clients[id]){
                    sendRoomClientList(clients[id].room);
                }
                break;
            case "room":
                clients[id].room = clientMsg.room;
                readFromStore(id);
                sendRoomClientList(clients[id].room);
                break;
            case "message":
                sendMessageAll(id,clientMsg);
                break;
            default:
                console.log(`сообщение не распознано: ${clientMsg.messageType}`);
                break
        }
    });

    ws.on('close', function ()  {
        if (clients[id]){
            clients[id].status = 'offline';
            clients[id].room = 1;
            // sendListClients();
            sendClientListRoom(clients[id].room);
            console.log(`client: ${clients[id].name}  status: ${clients[id].status}`);
        }
    });

    function editMessage(clientMsg) {
        for (let i = 0; i <= storage.length; i++){
            if (storage[i].id == clientMsg.msgId){
                storage[i].body = clientMsg.body;
                editToRoomById(clientMsg.msgId, clientMsg.body);
                return false;
            }
        }
    }
    function editToRoomById(msgId, body) {
        for (let key in clients){
            message = {
                messageType: 'editMsg',
                msgId: msgId,
                body: body,
                name: clients[key].name,
            };
            clients[key].connect.send(JSON.stringify(message));
        }
    }


    function deleteMessage(msgId) {
        for (let i = 0; i <= storage.length; i++){
            if (storage[i].id == msgId){
                storage.splice(i, 1);
                deleteMsgToRoomById(msgId);
                return false;
            }
        }
    }

    function deleteMsgToRoomById(msgId) {
        for (let key in clients){
            message = {
                messageType: 'deleteMsg',
                msgId: msgId,
            };
            clients[key].connect.send(JSON.stringify(message));
        }
    }

    function addUserToRoom(clientMsg) {
        let members = rooms[clientMsg.room].members;
        if (!members.includes(clientMsg.name)){
            rooms[clientMsg.room].members.push(clientMsg.name);
        }
        sendAllNoRoom();
    }

    function registrationClient(clientMsg) {
        for (var key in clients){
            if (clients[key].name === clientMsg.userName){
                if (clients[key].status === 'offline'){
                    id = clients[key].id;
                    clients[key].connect = ws;
                    clients[key].room = clientMsg.room;
                    clients[key].status = 'online';
                    console.log(`client: ${clients[key].name} вернулся`);
                    sendListRoomAll();
                    return false;
                } else {
                    ws.send(JSON.stringify({
                        messageType: 'already',
                    }));
                    console.log(`client: ${clients[key].name} уже сушествует`);
                    return  false;
                }
            }
        }
        clients[id] = new Client;
        clients[id].name = clientMsg.userName;
        clients[id].connect = ws;
        clients[id].id = id;
        clients[id].room = 1;
        clients[id].status = 'online';
        rooms[1].members.push(clients[id].name);
        console.log(`client: ${clients[id].name} зарегистритован`);
        return false;
    }
    function addNewRoom(){
        lastRoom++;
        rooms[lastRoom] = new Room();
        rooms[lastRoom].members.push(clients[id].name);
        rooms[lastRoom].owner = clients[id].name;

        console.log(`new room:${lastRoom} members: ${rooms[lastRoom].members} `);
        console.log(`creator: ${rooms[lastRoom].owner}`);
        sendListRoomAll();
    }
    function sendListRoomAll() {
        for (let key in clients){
            console.log(key);
            sendClientListRoom(key);
        }
    }
    function sendOwnerRoom() {
        clients[id].connect.send(JSON.stringify({
            messageType: 'owner',
            body: rooms[clients[id].room].owner,
        }))
    }
    function sendClientListRoom(client) {
        let listRooms = [];

        for (let room in rooms){
            if (clients[client]){
                if (rooms[room].members.includes(clients[client].name)){
                    listRooms.push({
                        room: room,
                    })
                }
            }
        }

        console.log(listRooms);
        if (listRooms.length){
            clients[client].connect.send(JSON.stringify({
                messageType: 'listRoom',
                body: listRooms,
            }))
        }
    }
    function sendAllNoRoom() {
        let listUsers =[];
        let members = rooms[clients[id].room].members;
        for (let key in clients){
            if (!members.includes(clients[key].name)){
                listUsers.push({
                    id: clients[key].id,
                    name: clients[key].name,
                })
            }
        }
        console.log(JSON.stringify(listUsers));
        clients[id].connect.send(JSON.stringify({
            messageType: 'allUser',
            body: listUsers,
        }))
    }

    
    function sendRoomClientList(room) { // отправляет список клиентов в комнате
        let listClient = [];

        let members = rooms[room].members;
        members.forEach(function (value) {
            let client = clientByName(value);
            listClient.push({
                name: value,
                status: client.status,
                rooms: client.room,
                owner: rooms[room].owner,
            })
        });
        members.forEach(function (value) {
            let client = clientByName(value);

            client.connect.send(JSON.stringify({
                messageType: 'list',
                body: listClient,
            }));
        });
    }

    function clientByName(name) {
        for (let key in clients){
            if (clients[key].name == name){
                return clients[key];
            }
        }
    }
});

function sendMessageAll(id,clientMsg){
    let idMsg = Math.random();
    for (var key in clients){
        if (clients[key].room == clientMsg.room){
            let msgToRoom = JSON.stringify({
                idMsg: idMsg,
                messageType: 'message',
                userName: clients[id].name,
                body: clientMsg.body,
            });
            clients[key].connect.send(msgToRoom);
        }
    }
    writeToStore(idMsg, clients[id].room, clients[id].name, clientMsg.body);
}
function writeToStore(id, room, name, body){ // body = name + msg
    storage.push({
        id: id,
        room: room,
        name: name,
        body: body,
    });
    // for (let st of storage){
    //     console.log(`storage: ${st.id}, room:${st.room}, ${st.name}:${st.body}`);
    // }
    // console.log('************************************************')
}
function readFromStore(key){
    // console.log(storage);
    for (let stored of storage ){
        // console.log(stored);
        // console.log(`stored.room: ${stored.room} clients[key].room: ${clients[key].room}`);

        if (stored.room == clients[key].room){
            clients[key].connect.send(JSON.stringify({
                messageType: 'message',
                idMsg: stored.id,
                userName: stored.name,
                body: stored.body,
            }));
        }
    }
}

